class CreateTableActivity < ActiveRecord::Migration[5.2]
  def change
    create_table :activities do |t|
      t.datetime    :due_date
      t.string      :description
      t.string      :status
      t.integer     :person_id
      t.integer     :creator_id
      t.timestamps
      t.integer     :version
    end
  end
end