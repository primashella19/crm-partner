
module ActivityFinder
  class MissingAttributeError < StandardError ;end
  def find_all_activities_by_person_id(person_id)
    if person_id.blank?
      raise MissingAttributeError.new("person id ga bole kosong")
    end
    # query = "select a.due_date as activity_due_date, a.description as activity_description,
    #             a.status as activity_status, m.name as member_name
    #           from activities a
    #           left join members m on a.creator_id = m.id
    #           where a.creator_id = #{person_id}"
    # return ActiveRecord::Base.connection.exec_query(query)
    return  Activity.where("person_id = ? ", person_id ).
              joins("join members m on m.id = activities.creator_id").
              select("activities.id as id , activities.due_date as activity_due_date, activities.description as activity_description, activities.status as activity_status, m.name as member_name")
  end
end
