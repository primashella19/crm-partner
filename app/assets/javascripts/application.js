// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require bootstrap-datepicker
//= require select2-full
//= require activestorage
//= require turbolinks
//= require confirm-modal
//= require table-ajax
//= require infinite-scroll
//= require sync-jurnal

$(document).on("turbolinks:load", function(){
  $('[data-toggle="tooltip"]').tooltip();
  $('.dropdown-toggle').dropdown();
  
  $( ".select2-option" ).select2({
    theme: "bootstrap"
  });

  $(".select2-tags").select2({
    tags: true,
    tokenSeparators: [',', ';']
  });

  /* hide select2-tags search results, 
  // select2 data option minimumSearchResults only works for single select2 */
  $(".select2-tags").on('select2:open', function( event ) {
    $(".select2-dropdown").css({
      "border": "0",
      "border-top": "1px solid #66afe9",
      "background-color": "transparent"
    });
    $(".select2-results").css("display", "none");
  });

  $("#collapseFilter").on("show.bs.collapse", function(){
    $(".collapse-out, .collapse-in", "#collapseIcon").toggleClass("hidden");
  });

  $("#collapseFilter").on("hide.bs.collapse", function(){
    $(".collapse-out, .collapse-in", "#collapseIcon").toggleClass("hidden");
  });
});