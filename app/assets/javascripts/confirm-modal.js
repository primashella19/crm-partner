//Override the default confirm dialog by rails
$.rails.allowAction = function(link){
  if (link.data("confirm") == undefined){
    return true;
  }
  $.rails.showConfirmationDialog(link);
  return false;
}
//User click confirm button
$.rails.confirmed = function(link){
  link.data("confirm", null);
  link.trigger("click.rails");
}
//Display the confirmation dialog
$.rails.showConfirmationDialog = function(link){
  var title = link.data("title");
  var message = link.data("confirm");
  var confirm_btn = link.data("confirm-btn");
  var confirm_btn_class = link.data("confirm-btn-class");
  var modal = "#popup-confirm";

  $(modal).modal("show");
  $(".title-row", modal).html(title);
  $(".modal-body", modal).html(message);
  $("#btn-confirm").text(confirm_btn);
  $("#btn-confirm").removeClass().addClass(confirm_btn_class);
  $("#btn-confirm", modal).on("click", function(){
    $.rails.confirmed(link);
  });
}