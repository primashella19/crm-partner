$(document).on("turbolinks:load", function(){
  const $modal = $("#popup-sync-jurnal");
  const $modal_loading = $(".modal-body-loading", "#popup-sync-jurnal");
  const $modal_success = $(".modal-body-success", "#popup-sync-jurnal");
  const $modal_msg_success = $(".message-success", "#popup-sync-jurnal");
  const $sync_btn = $("#popup-sync-jurnal-btn");
  const $sync_btn_wrapper = $("#sync-jurnal-wrapper");
  const $see_list_btn = $("#see-list-btn");

  $sync_btn_wrapper.tooltip('disable');

  function toggleSyncBtn($btn, value){
    if(value){
      $sync_btn.attr("disabled", true);
      $sync_btn_wrapper.tooltip('enable');
      $sync_btn.find(".glyphicon-refresh").addClass("glyphicon-refresh-animate");
    }else{
      $sync_btn.removeAttr("disabled");
      $sync_btn_wrapper.tooltip('disable');
      $sync_btn.find(".glyphicon-refresh").removeClass("glyphicon-refresh-animate");
    }
  }

  $sync_btn.on("click", function(){
    $modal.modal("show");
    toggleSyncBtn($sync_btn, true);
    $modal_loading.removeClass("hidden");
    $modal_success.addClass("hidden");

    $.ajax({
      type: $modal.attr("data-method"),
      url: $modal.attr("data-action"),
      success: function(data){
        console.log(data);
        $modal.modal("show");
        $modal_msg_success.html(data.results);
        $modal_success.removeClass("hidden");
        $modal_loading.addClass("hidden");
        toggleSyncBtn($sync_btn, false);
      },
      error: function(xhr){
        console.log(xhr)
        toggleSyncBtn($sync_btn, false);
      }
    });
  });

  $see_list_btn.on("click", function(){
    var filter_value = $see_list_btn.attr("data-filter-value");
    var filter_el = "input[value=" + filter_value + "]";
    $(filter_el, "#form-search").prop("checked", "checked");
    $("#form-search").submit();
  });
});