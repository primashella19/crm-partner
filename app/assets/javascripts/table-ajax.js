function submitAjaxForm($form, $target){
  var dataSet = $form.serialize();
  return new Promise(function(resolve, reject){
    $.ajax({
      type: $form.attr("method"),
      url: $form.attr("action"),
      data: dataSet,
      success: function(data){
        if($target){
          $target.html(data);
        }
        resolve(data);
      },
      error: function(xhr){
        resolve(false);
      }
    });
  });
}

function resetSearchForm($form){
  $form[0].reset();
  $(".sort-header").attr("data-sort", "desc");
}

function updatePageInfo(){
  var result_count = $("tr", "#target-form-result tbody").length;
  $("#page-info-current-data").text(result_count);
}

function submitFormWithPagination($this){
  var $target = $("#page_no", "#form-search");
  var $page_input = $("#page-value");
  var last_page = parseInt($page_input.data("last-page"));
  var curr_page = parseInt($page_input.val());
  if($this.data('type') == "static"){
    var total_page = parseInt($this.data('value') || $this.val());
  }else{
    var total_page = curr_page + parseInt($this.data('value') || $this.val());
  }

  if(total_page <= 0){
    total_page = 1;
  }

  if(total_page > last_page){
    total_page = last_page;
  }

  $target.val(total_page);
  $page_input.val(total_page);

  var $form = $("#form-search");
  var $target = $($form.data("ajax-target"));

  submitAjaxForm($form, $target).then(function(value){
    updatePageInfo();
  });
}

function toggleSorting($this){
  if($this.dataset.sort == "desc"){
    $($this.dataset.el).val("asc");
    $this.dataset.sort = "asc";
  }else if($this.dataset.sort == "asc"){
    $($this.dataset.el).val("desc");
    $this.dataset.sort = "desc";
  }

  var $form = $("#form-search");
  var $target = $($form.data("ajax-target"));
  submitAjaxForm($form, $target);
}

$(document).on("turbolinks:load", function(){
  $("#form-search").on("submit", function(e){
    e.preventDefault();
    var $this = $(this);
    var $target = $($(this).data("ajax-target"));
    submitAjaxForm($this, $target).then(function(value){
      updatePageInfo();
    });
    return false;
  });

  $("table").on("click", "thead th.sort-header", function(){
    toggleSorting(this);
  });

  $("#page-value").on("keypress", function(e){
    var $this = $(this);
    var keycode = (e.keyCode ? e.keyCode : e.which);
    if(keycode == '13'){
      submitFormWithPagination($this);
    }
  });

  $(".go-page").on("click", function(e){
    e.preventDefault();

    var $this = $(this);
    submitFormWithPagination($this);
  });

  $("#form-create-lead, #form-create-client").on("submit", function(e){
    e.preventDefault();
    var $form = $(this);
    var $target = $($form.data("ajax-target"));
    submitAjaxForm($form, $target).then(function(value){
      $(".modal").modal("hide");
      resetSearchForm($("#form-search"));
      resetSearchForm($form);
      updatePageInfo();
      
      if(value == false){
        alert($form.data("error-msg"));
      }else{
        alert($form.data("success-msg"));
      }
    });
  });

  $("#form-update-lead, #form-update-client").on("submit", function(e){
    e.preventDefault();
    var $form = $(this);
    submitAjaxForm($form).then(function(value){
      $(".modal").modal("hide");
      if(value == false){
        alert($form.data("error-msg"));
      }else{
        alert($form.data("success-msg"));
      }
    });
  });

  $("#form-create-activity, #form-create-member").on("submit", function(e){
    e.preventDefault();
    var $form = $(this);
    var $target = $($form.data("ajax-target"));
    submitAjaxForm($form, $target).then(function(value){
      $(".modal").modal("hide");
    });
  });


  $(".form-render-update-activity-modal").on("submit", function(e){
    e.preventDefault();
    $("#form-update-activity").detach();
    var $form = $(this);
    var $target = $($form.data("ajax-target"));
    submitAjaxForm($form, $target).then(function(value){
      $("#popup-update-activity").modal("show");
    });
  });

  $("body").on("submit", ".form-render-sync-modal", function(e){
    e.preventDefault();
    $("#form-sync-client").detach();
    var $form = $(this);
    var $target = $($form.data("ajax-target"));
    submitAjaxForm($form, $target).then(function(value){
      $( ".select2-option" ).select2({
        theme: "bootstrap"
      });
      $("#popup-sync-client").modal("show");
    });
  });

  $("body").on("submit", "#form-update-activity", function(e){
    e.preventDefault();
    submitAjaxForm($(this)).then(function(data){
      var $row = $("tr[data-activity-id=" +  data.id +"]");
      $row.find(".activity-desc").text(data.description);

      // todo later logic status color
      $row.find(".status-color").removeClass("grey").addClass("yellow");
      $("#popup-update-activity").modal("hide");
    });
  });
});