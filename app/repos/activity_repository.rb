class ActivityRepository
  include ActivityFinder

  attr_accessor :activity_repo

  def initialize()
    super

    @activity_repo = []
  end

  def load(id: nil)
    data_to_return = {}
    if id.present?
      activity_to_load = Activity.find_or_initialize_by(id: id)
      if activity_to_load.present?
        data_to_return = {id: activity_to_load.id,
                            person_id: activity_to_load.person.id,
                            person_name: activity_to_load.person_name
                          }
      end
    end

    # return self.activity_repo
  end

  def save(activity_variables = {})
    if activity_variables.blank?
      raise "activity variables harus di kirim"
    end

    activity_to_save = Activity.find_or_initialize_by(id: activity_variables[:id])
    activity_to_save.with_lock do |data|
      activity_to_save.person_id = activity_variables[:lead_id]
      activity_to_save.description = activity_variables[:description]
      activity_to_save.status = activity_variables[:status]
      activity_to_save.due_date = activity_variables[:due_date]
      activity_to_save.creator_id = activity_variables[:creator_id]
      activity_to_save.version = activity_to_save.version.to_i + 1
      activity_to_save.save!
    end
  end

  def load_collection(person_id)
    list_of_activities = {}
    if person_id.present?
      list_of_activities = find_all_activities_by_person_id(person_id)
    end

    return list_of_activities
  end

  #delete later and must be merged with save_create
  def save_create(activity_params)
    activity_to_create = Activity.find_or_initialize_by(activity_params[:id], true)
    activity_to_create.register_activity_attributes(activity_params)
    if activity_to_create.is_valid_new_activity? == true
      activity_to_create.id = 2
      @activity_repo << activity_to_create
    end

    return activity_to_create
    #activity_to_update = Activity.find_or_initialize_by(id: company_group_variables[:id])

    # implement later, cannot save yet
    # activity_to_update.with_lock do
    #   activity.save!
    # end
  end

  #delete later and must be merged with save_create
  def save_update(activity_params)
    activity_to_update = Activity.find_or_initialize_by(activity_params[:id], false)

    if activity_to_update.is_valid
    #activity_to_update = Activity.find_or_initialize_by(id: company_group_variables[:id])

    # implement later, cannot save yet
    # activity_to_update.with_lock do
    #   activity.save!
    end
  end
end