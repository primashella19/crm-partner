class LeadRepository
  def load_collection_by(member_id: nil)

  end

  def load(id: nil)
    return Lead.new(id: 1,
                          company_name: "syalala",
                          contact_name: "syalala asdasad",
                          phone: "syalala",
                          email: "syalala@mail.com",
                          industry: "syalala industry",
                          address: "j alan syalala",
                          description: " lalala alala syalala",
                          member_id: id,
                          status: "open"
                          )
    if id.blank?
      raise
    end
  end

  def save(lead_variables = {})
    if lead_variables.blank?
      raise "ga bole kosong"
    end
    lead_to_save = lead.find_or_initialize_by(id: lead_variables[:id])
    lead_to_save.with_lock do |lead|
      lead.assign_attributes(
                              company_name: lead_variables[:company_name],
                              contact_name: lead_variables[:contact_name],
                              phone: lead_variables[:phone],
                              email: lead_variables[:email],
                              industry: lead_variables[:industry],
                              address: lead_variables[:address],
                              description: lead_variables[:description],
                              member_id: lead_variables[:member_id],
                              status: lead_variables[:status]
                              )
      lead.save!
    end
  end
end