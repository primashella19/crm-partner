# ini sementara pake virtus model dulu, karena pake form for di front end nya
class Lead
  include ActiveModel::Model
  include Virtus::Model

  attribute :company_name, String
  attribute :contact_name, String
  attribute :phone, String
  attribute :email, String
  attribute :status, String
  attribute :industry, String
  attribute :address, String
  attribute :description, String
  attribute :member_id, Integer
  attribute :id, Integer
end