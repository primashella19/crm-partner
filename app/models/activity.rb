class Activity < ActiveRecord::Base
  def initialize(params={})
    super

    @created_at = DateTime.now
    @updated_at = DateTime.now
    @version = 0
  end
end