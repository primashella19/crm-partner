class MemberMailer < ApplicationMailer
  def invite(target_email)
    @target_email = target_email
    @user = "Daniel Senjoyo"
    @company = "Best Consultant Company"
    mail from: "no-reply-crm-partner@jurnal.id",
           to: @target_email,
           subject: "Invite Member"
  end
end
