class ActivitiesController < ApplicationController
  before_action :authenticate_member!

  def show
    @total_pages = 4
    @current_pages = 1
    activity_repo
    @lists = @repo.load_collection(params[:id])
    render partial: 'activities/lists'
  end

  def new
  end

  def create
    params[:status] || "false" #checkbox ngirimny bentuk string
    params[:due_date] || nil
    params[:description] || nil
    if params[:status].present? #temporary, later change to if model.save
      redirect_to lead_path(params[:lead_id])
    else
      redirect_to lead_path(params[:lead_id])
    end
  end

  def update
    render json: {id: 1, status: "true", due_date: Time.now, description: "Random Cat"}
  end

  def destroy
    redirect_to lead_path(params[:lead_id])
  end

  def render_update_modal
    @activity = Activity.find(4)
    render :partial => 'popup_update_activity'
  end

  def activity_repo
    @repo = ActivityRepository.new
  end
end
