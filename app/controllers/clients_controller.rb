class ClientsController < ApplicationController
  before_action :authenticate_member!
  before_action :init_repo_queries, :current_user

  def index
    @last_page = 10
    @current_data = 1 #@query_results.count
    @total_data = 25
    @filter_value = 4 #todo temporary, value for need to link filter option
  end

  def create
    render :partial => 'lists', locals: {choice: "anything else", sort: "desc"}
  end

  def show
    @lists = @query_results.take(4)
    @total_pages = 4
    @client = Lead.new(1, SecureRandom.base64(8), ["email a", "email b"], "081222", "Archived", "Sakura")
    @clients_company = @lists
  end

  def search
    if params[:search_sort] == "desc"
      @query_results = @query_results.sort_by{|x| x.company_name.downcase}.reverse!
    else
      @query_results = @query_results.sort_by{|x| x.company_name.downcase}
    end
    
    render :partial => 'lists', locals: {choice: params[:choice], sort: params[:search_sort]}
  end

  def update
    params[:id] # lead = Client.find(params[:id])
    params[:status_id] # update if param exists
    redirect_to client_path(params[:id])
  end

  def destroy
    redirect_to clients_path
  end

  def render_sync_modal
    @client = Lead.new(params[:id], SecureRandom.base64(8), ["email a", "email b"], "081222", "new lead", "Budi")
    @clients_list = @query_results
    render :partial => 'popup_sync_client', locals: {company_name: @client.company_name}
  end

  def sync_jurnal
    sleep 5
    render json: {results: t("clients.sync-jurnal-count", count: 4).html_safe}
  end

  def unsync_jurnal
    #todo nanti cm owner yg bisa ini
    redirect_to client_path(params[:client_id])
  end

  def init_repo_queries
    @industry_lists = [["Retail/Online Shop", 1], ["Jasa", 2], ["Konstruksi", 2]]
    @client_status_filter = [{id: 1, name: "All", count: 23}, 
                            {id: 2, name: "Jurnal", count: 11},
                            {id: 3, name: "Non Jurnal", count: 77},
                            {id: 4, name: "Need to Link", count: 12},
                            {id: 5, name: "Archived", count: nil}]

    @query_results = [
        Lead.new(1, SecureRandom.base64(8), ["email a", "email b"], "081222", "new lead", "Budi"), 
        Lead.new(2, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(2, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(2, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(2, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(4, "hello there", ["email a", "email b"], "081333", "new lead", "Budi")]

    @client_status = [["Archived", "Archived"], ["Active", "Active"]]
  end

  def current_user
  end
end
