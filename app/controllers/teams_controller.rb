class TeamsController < ApplicationController
  before_action :authenticate_member!
  before_action :init_repo_queries

  def index
    @last_page = 10
    @current_data = 1 #@query_results.count
    @total_data = 25
  end

  def create
    MemberMailer.invite(params["email"]).deliver_now
  end

  def show
    @lists = @query_results.take(4)
    @total_pages = 4
    @team = Lead.new(1, SecureRandom.base64(8), ["email a", "email b"], "081222", "Archived", "Sakura")
    @teams_company = @lists
  end

  def search
    if params[:search_sort] == "desc"
      @query_results = @query_results.sort_by{|x| x.company_name.downcase}.reverse!
    else
      @query_results = @query_results.sort_by{|x| x.company_name.downcase}
    end
    
    render :partial => 'lists', locals: {choice: params[:choice], sort: params[:search_sort]}
  end

  def reactive
    redirect_to team_path(params[:id])    
  end

  def update
    params[:id] # lead = Client.find(params[:id])
    params[:status_id] # update if param exists
    redirect_to team_path(params[:id])
  end

  def destroy
    redirect_to clients_path
  end

  def init_repo_queries
    @industry_lists = [["Retail/Online Shop", 1], ["Jasa", 2], ["Konstruksi", 2]]

    @team_status_filter = [{id: 1, name: "All", count: 23}, 
                            {id: 2, name: "Invited", count: 5},
                            [{id: 3, name: "Active", count: 12}, 
                              [{id: 4, name: "Admin", count: 6},
                              {id: 5, name: "Staff", count: 1}]
                            ],
                          {id: 6, name: "Non-Active", count: 23}]

    @query_results = [
        Lead.new(1, SecureRandom.base64(8), ["email a", "email b"], "081222", "new lead", "Budi"), 
        Lead.new(2, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(2, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(2, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(2, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(4, "hello there", ["email a", "email b"], "081333", "new lead", "Budi")]

    @team_status = [["Archived", "Archived"], ["Active", "Active"]]
  end
end
