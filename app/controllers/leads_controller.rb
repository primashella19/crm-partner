require "#{Rails.root}/lib/domain/leads/leads"

class LeadsController < ApplicationController
  before_action :authenticate_member!
  before_action :init_repo_queries

  def index
    @last_page = 10
    @current_data = 1 #@query_results.count
    @total_data = 25
  end

  def create
    render :partial => 'lists', locals: {sort: "desc"}
  end

  def show
    @total_pages = 4
    lead_repository
    @lead = @repo.load(id: params[:id])
  end

  def update
    params[:id] # lead = Lead.find(params[:id])
    params[:status_id] # update lead status with this
    redirect_to lead_path(params[:id])
  end

  def destroy
    redirect_to leads_path
  end

  def convert_client
    #create new client, get id
    new_client_id = params[:id]
    redirect_to client_path(new_client_id)
  end

  def search
    if params[:search_sort] == "desc"
      @query_results = @query_results.sort_by{|x| x.company_name.downcase}.reverse!
    else
      @query_results = @query_results.sort_by{|x| x.company_name.downcase}
    end

    render :partial => 'lists', locals: {sort: params[:search_sort]}
  end

  # remove later
  def init_repo_queries
    @industry_lists = [["Retail/Online Shop", 1], ["Jasa", 2], ["Konstruksi", 2]]
    @leads_status_filter = [{id: 1, name: "All", count: 23},
                      {id: 2, name: "New Leads", count: 4},
                      {id: 3, name: "In Progress", count: 5},
                      [{name: "Closed"},
                        [{id: 5, name: "Won", count: 6},
                          {id: 6, name: "Lost", count: 1},
                          {id: 7, name: "Junk", count: 2},
                          {id: 8, name: "Unreachable", count: 0},
                          {id: 9, name: "Underqualified", count: 11},
                          {id: 10, name: "Void/Duplicate", count: 2},
                          {id: 11, name: "Product/Feature", count: 3}]]]

    @leads_status = [["new lead", "new lead"], ["closed", "closed"], ["in progress", "in progress"], ["won", "won"]]
  end

  def lead_repository
    @repo = LeadRepository.new
  end
end
