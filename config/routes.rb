Rails.application.routes.draw do
  root "leads#index"

  devise_for :members, path: '', controllers: {
    confirmations: 'members/confirmations',
    passwords: 'members/passwords',
    registrations: 'members/registrations',
    sessions: 'members/sessions',
    unlocks: 'members/unlocks'
  }

  devise_for :admins, path: 'admin', controllers: {
    confirmations: 'admins/confirmations',
    passwords: 'admins/passwords',
    registrations: 'admins/registrations',
    sessions: 'admins/sessions',
    unlocks: 'admins/unlocks'
  }

  namespace :admins do
    get "/", to: "admins#index"
    resources :partners
    resources :leads
  end
  
  resources :leads do
    collection do
      post :search
    end
    member do
      post :convert_client
    end
  end

  resources :clients do
    collection do
      post :search
      get :sync_jurnal
    end
    member do
      post :render_sync_modal
      post :unsync_jurnal
    end
  end

  resources :teams do
    collection do
      post :search
    end
    member do
      patch :reactive
    end
  end
  
  resources :activities do
    member do
      post :render_update_modal
    end
  end

  get 'health', to: 'health#show'
end
